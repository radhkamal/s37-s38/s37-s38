// Today's Deliverables
//   - Data Model Design
//   - User Registration
  
// E-commerce API MVP requirements:
//  - User Registration
//  - User Authentication
//  - Set User as Admin (Admin only)
//  - Retrieve All Active Products
//  - Retrieve Single Product
//  - Create Product (Admin only)
//  - Update Product Information (Admin only)
//  - Archive Product (Admin only)
//  - Non-Admin User Checkout (Create Order)
//  - Retrieve Authenticated User’s Orders
//  - Retrieve All Orders (Admin only)

  
// User Credentials:
//   - Admin User:
//     -- email: admin@email.com
//     -- password: admin123
//   - Dummy Customer
//     -- email: customer@mail.com
//     -- password: customer123

  
// Data Model Requirements
//   - User
//     - Email (String)
//     - Password (string)
//     - isAdmin (Boolean - defaults to false)
    
//   - Product
//     - Name (String)
//     - Description (String)
//     - Price (Number)
//     - isActive (Boolean - defaults to true)
//     - createdOn (Date - defaults to current date of creation)
    
//   - Order
//     - totalAmount (Number)
//     - purchasedOn (Date - defaults to current date of creation)
//     -  Must be associated with:
//       --  A user who owns the order
//       --  Products that belong to the order

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product name is required."]
	},

	description: {
		type: String,
		required: [true, "Description is required."]
	},

	price: {
		type: Number,
		required: [true, "Price is required."]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

    orders: [
        {
            userId: {
                type: String,
                required: [true, "UserId is required."]
            },

			purchasedOn: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Pending Payment"
			}
        }
    ]
})

module.exports = mongoose.model("Product", productSchema);
