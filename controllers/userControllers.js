const User = require("../models/User");
const bcrypt = require("bcrypt");
const req = require("express/lib/request");
const auth = require("../authentication/auth");

//Check if Email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then( result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


//User Registration
module.exports.registerUser = (reqBody) => {

		let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
                isAdmin: reqBody.isAdmin,
				password: bcrypt.hashSync(reqBody.password, 10)
		})

		return newUser.save().then((user, error) => {

			if(error){
				return false
			} else{
				return true
			}
		})
}


//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result =>{

		if(result == null){
			return false
		} else {

			//compareSync(dataToBeCompared, encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

module.exports.makeAdmin = (data) => {
    return User.findById(data.userId).then((result, error) => {
        console.log(result);

        if(data.isAdmin == true) {
            result.isAdmin = true;
            console.log(result);

            return result.save().then((updatedUser, error) => {

				if(error){
					return false;
				} else {
					return updatedUser;
				}
			})
        } else {
            return "Please login to an Admin account before setting others as Admin.";
        }
    })
}